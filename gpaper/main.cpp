#include <iostream>
#include <vector>

#include <Windows.h>

#include "SDL.h"
#include "GL/glew.h"

#include "nanovg.h"

#define NANOVG_GL3_IMPLEMENTATION
#include "nanovg_gl.h"

#include "gpaper.h"

BOOL CALLBACK find_worker(HWND wnd, LPARAM lp) {
	HWND* pworker = (HWND*) (lp);

	if(!FindWindowExA(wnd, 0, "SHELLDLL_DefView", 0)) {
		return TRUE;
	}

	*pworker = FindWindowExA(0, wnd, "WorkerW", 0);

	if(*pworker) {
		return FALSE;
	}

	return TRUE;
}

HWND get_worker() {
	std::cout << "Getting progman" << std::endl;

	HWND progman, worker;

	progman = FindWindowA("Progman", 0);

	if(!progman) {
		std::cout << "Failed to get progman" << std::endl;
		return 0;
	}

	std::cout << "Sending messages..." << std::endl;

	SendMessageA(progman, 0x052C, 0xD, 0);
	SendMessageA(progman, 0x052C, 0xD, 1);

	EnumWindows(find_worker, (LPARAM)&worker);

	if(!worker) {
		std::cout << "Couldn't find worker! Trying with alt method..." << std::endl;

		SendMessageA(progman, 0x052C, 0, 0);
		EnumWindows(find_worker, (LPARAM)&worker);

		if(worker) {
			ShowWindow(worker, SW_HIDE);
			worker = progman;
		} else {
			std::cout << "This didn't work either, using progman!" << std::endl;
			worker = progman;
		}
	}

	return worker;
}

std::string vformat(const char *fmt, va_list ap) {
	// Allocate a buffer on the stack that's big enough for us almost
	// all the time.  Be prepared to allocate dynamically if it doesn't fit.
	size_t size = 1024;
	char stackbuf[1024];
	std::vector<char> dynamicbuf;
	char *buf = &stackbuf[0];
	va_list ap_copy;

	while(1) {
		// Try to vsnprintf into our buffer.
		va_copy(ap_copy, ap);
		int needed = vsnprintf(buf, size, fmt, ap);
		va_end(ap_copy);

		// NB. C99 (which modern Linux and OS X follow) says vsnprintf
		// failure returns the length it would have needed.  But older
		// glibc and current Windows return -1 for failure, i.e., not
		// telling us how much was needed.

		if(needed <= (int)size && needed >= 0) {
			// It fit fine so we're done.
			return std::string(buf, (size_t)needed);
		}

		// vsnprintf reported that it wanted to write more characters
		// than we allotted.  So try again using a dynamic buffer.  This
		// doesn't happen very often if we chose our initial size well.
		size = (needed > 0) ? (needed + 1) : (size * 2);
		dynamicbuf.resize(size);
		buf = &dynamicbuf[0];
	}
}

std::string format(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	std::string buf = vformat(fmt, ap);
	va_end(ap);
	return buf;
}

int main(int argc, char** argv) {
	HWND worker = get_worker();
	
	if(!worker) {
		std::cout << "Couldn't get worker! RIP" << std::endl;
		return 1;
	}

	SDL_InitSubSystem(SDL_INIT_VIDEO);

	SDL_Window* awin = SDL_CreateWindow(
		"Live Wallpaper Alt",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		512, 512,
		SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN
	);
	
	SDL_SetHint(SDL_HINT_VIDEO_WINDOW_SHARE_PIXEL_FORMAT, format("%p", awin).c_str());

	SDL_Window* sdlWnd = SDL_CreateWindowFrom(worker);
	SDL_SetWindowTitle(sdlWnd, "Live Wallpaper");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_CreateContext(sdlWnd);

	glewExperimental = GL_TRUE;
	glewInit();
	
	int width, height;
	SDL_GetWindowSize(sdlWnd, &width, &height);

	struct NVGcontext* vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES);

	while(true) {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		nvgBeginFrame(vg, width, height, 1);

		gp_draw(vg, width, height, SDL_GetTicks() / 1000.0f);

		nvgEndFrame(vg);

		SDL_GL_SwapWindow(sdlWnd);
		SDL_Delay(1000 / 60);
	}

	SDL_Quit();
	get_worker(); // this resets the wallpaper

	return 0;
}
