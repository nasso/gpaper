#pragma once

#include "nanovg.h"

void gp_init();
void gp_draw(struct NVGcontext* vg, const int w, const int h, const float t);
