#include "gpaper.h"
#include "nanovg.h"

#include <cstdlib>

const float SPEED = 10.0f;
const int PCOUNT = 1024;
const float MAX_DIST = 130.0f;

static float rnd() {
	return float(rand()) / float(RAND_MAX + 1);
}

static float modulus(const float x, const float y) {
	return x - y * floorf(x / y);
}

void gp_draw(struct NVGcontext* vg, const int w, const int h, float t) {
	srand(0x4991);

	float px[PCOUNT];
	float py[PCOUNT];

	for(int i = 0; i < PCOUNT; i++) {
		float dt = (t * (1.0f - rnd())) + rnd() * 1000.0f;

		// gen rand vec
		float dx = rnd() * 2.0f - 1.0f;
		float dy = rnd() * 2.0f - 1.0f;
		float dn = sqrtf(dx * dx + dy * dy);
		dx /= dn;
		dy /= dn;

		float x = (rnd() - 0.5f) * w;
		float y = dy * dt * SPEED;

		x += dx * dt * SPEED;

		px[i] = modulus(x, w);
		py[i] = modulus(y, h);
	}

	nvgStrokeWidth(vg, 1.0f);
	nvgLineCap(vg, NVG_ROUND);
	nvgLineJoin(vg, NVG_ROUND);

	for(int i = 0; i < PCOUNT - 1; i++) {
		float ax = px[i];
		float ay = py[i];
		
		for(int j = i + 1; j < PCOUNT; j++) {
			float bx = px[j];
			float by = py[j];

			float dist = sqrtf((ax - bx) * (ax - bx) + (ay - by) * (ay - by));

			if(dist < MAX_DIST) {
				nvgBeginPath(vg);
				nvgMoveTo(vg, ax, ay);
				nvgLineTo(vg, ax + 1, ay);
				nvgLineTo(vg, bx, by);
				nvgLineTo(vg, bx + 1, by);
				
				// nvgArc(vg, 0.5f * (ax + bx), 0.5f * (ay + by), 0.5f * dist, 0, 2.0f * NVG_PI, NVG_CW);

				nvgStrokeColor(vg, nvgRGBAf(1.0f, 1.0f, 1.0f, (1.0 - dist / MAX_DIST) * 0.25f));
				nvgStroke(vg);
			}
		}
	}
}

